
set -e

CUR_PATH=`pwd`
echo "build path: $CUR_PATH"
if [ "$(basename $CUR_PATH)" != "build" ];then
	echo "please run in path 'build'"
	exit 1
fi

BUILD_TYPE=Debug
INSTALL_PATH="$(dirname $CUR_PATH)"/install-$BUILD_TYPE

echo "install path: $INSTALL_PATH"
echo "build log: $CUR_PATH/build.log"
echo "install log: $CUR_PATH/install.log"
echo "start build ..."

#CC=/media/hzq/jj/llvm-project/install-release/bin/clang
#CXX=/media/hzq/jj/llvm-project/install-release/bin/clang++
CC=/usr/bin/gcc
CXX=/usr/bin/g++

sudo cmake \
	../llvm \
	-G "Ninja" \
	-DLLVM_TARGETS_TO_BUILD=X86 \
	-DLLVM_ENABLE_PROJECTS="clang;lld;compiler-rt" \
	-DCMAKE_BUILD_TYPE=$BUILD_TYPE \
	-DCMAKE_C_COMPILER=$CC \
	-DCMAKE_CXX_COMPILER=$CXX \
	-DCMAKE_C_FLAGS="-w" \
	-DCMAKE_CXX_FLAGS="-w -std=c++11" \
	-DCMAKE_INSTALL_PREFIX=$INSTALL_PATH

export PATH=$INSTALL_PATH:$PATH
ninja -j16 2>&1 | tee build.log
ninja install 2>&1 | tee install.log
