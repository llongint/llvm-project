# LLVM 编译器结构

该目录及其子目录包含LLVM的源代码，LLVM是用于构建高度优化的编译器，优化器和运行时环境的工具包。 自述文件简要介绍了如何开始构建LLVM。有关如何为LLVM项目做出贡献的更多信息，请查看[“对LLVM做出贡献”](https://llvm.org/docs/Contributing.html)指南。


## 开始使用 LLVM 系统
参考 [https://llvm.org/docs/GettingStarted.html](https://llvm.org/docs/GettingStarted.html)

### 概述

欢迎使用 LLVM 项目!

LLVM 项目包含很多部分，核心部分跟本身一样叫 “LLVM”，它包含处理中间表示并将其转换为目标文件所需的所有工具、库、头文件。工具包括汇编程序，反汇编程序，位代码分析器和位代码优化器。它还包含基本的回归测试。

类似C的语言使用[Clang](http://clang.llvm.org/)前端。该组件将C，C ++，Objective-C和Objective-C ++代码编译为LLVM位代码-并使用LLVM从那里编译为目标文件。

其他组成部分包括：
- [libc++ C++ standard library](https://libcxx.llvm.org),
- [LLD linker](https://lld.llvm.org), 等.

### LLVM 源码获取与构建
开始页的文档或许已经过时了，[clang 的入门文档](http://clang.llvm.org/get_started.html)可能包含更新的信息。

这是一个获取和构建LLVM源的示例工作流程和配置：

1. 检出 LLVM (包括类似 clang 的关联子项目):

     * ``git clone https://github.com/llvm/llvm-project.git``

     * 或者，在windows上, ``git clone --config core.autocrlf=false
    https://github.com/llvm/llvm-project.git``

2. 设置和构建 LLVM 和 Clang:

     * ``cd llvm-project``

     * ``cmake -S llvm -B build -G <generator> [options]``

        一些常见的构建系统生成器是:

        * ``Ninja`` --- 用于生成 [Ninja](https://ninja-build.org)
          构建文件. 大多数 llvm 开发者使用 Ninja.
        * ``Unix Makefiles`` --- 用于生成与make兼容的makefile.
        * ``Visual Studio`` --- 用于生成 Visual Studio项目和解决方案.
        * ``Xcode`` --- 用于生成 Xcode 项目.

        一些常见选项:

        * ``-DLLVM_ENABLE_PROJECTS='...'`` --- 您要另外构建的LLVM子项目的列表，以分号分隔。可以包括以下任何一项：clang，clang-tools-extra，libcxx，libcxxabi，libunwind，lldb，compiler-rt，lld，poly或debuginfo-tests.

          举个例子, 要构建 LLVM, Clang, libcxx, and libcxxabi, 可以这样：
          ``-DLLVM_ENABLE_PROJECTS="clang;libcxx;libcxxabi"``.

        * ``-DCMAKE_INSTALL_PREFIX=directory`` ---指定要在其中安装LLVM工具和库的完整路径名(默认是 ``/usr/local``).

        * ``-DCMAKE_BUILD_TYPE=type`` --- 合法的选项是 Debug,
          Release, RelWithDebInfo, 和 MinSizeRel. 默认是 Debug.

        * ``-DLLVM_ENABLE_ASSERTIONS=On`` --- 在启用断言检查的情况下进行编译（对于Debug版本，默认值为Yes；对于所有其他版本类型，默认值为No）.

      * ``cmake --build build [-- [options] <target>]`` 或直接在上面指定的构建系统.

        * 默认目标 (比如. ``ninja`` or ``make``) 会构建 LLVM 的所有项目.

        * ``check-all`` 目标 (比如. ``ninja check-all``) 或直接在上面指定的构建系统.

        * CMake 将为每个工具和库生成目标，并且大多数LLVM子项目都会生成自己的 ``check-<project>`` 目标

        * 运行串行构建会 **很慢**。为了提高速度，请尝试运行并行构建。默认情况下，这是在 Ninja 中完成的；对于make，请使用选项-j NNN，其中NNN是并行作业的数量，例如您拥有的CPU数量

      * 更多信息请参见 [CMake](https://llvm.org/docs/CMake.html)

有关配置和编译LLVM的详细信息，请查阅 [LLVM 开始页面](https://llvm.org/docs/GettingStarted.html#getting-started-with-llvm)。您可以访问 [目录布局](https://llvm.org/docs/GettingStarted.html#directory-layout)以了解源代码树的布局
