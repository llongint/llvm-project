

# [LLVM教程：目录](https://llvm.org/docs/tutorial/index.html)

## Kaleidoscope：使用LLVM实现语言

[我的第一个语言前端 LLVM教程](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/index.html)
> 这是 Kaleidoscope 语言教程，展示了如何使用 C++中的LLVM组件实现简单的语言

- [01. Kaleidoscope: Kaleidoscope 介绍与 Lexer](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/LangImpl01.html)
- [02. Kaleidoscope: 实现解析器和AST](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/LangImpl02.html)
- [03. Kaleidoscope: LLVM IR的代码生成](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/LangImpl03.html)
- [04. Kaleidoscope: 添加JIT和优化器支持](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/LangImpl04.html)
- [05. Kaleidoscope: 扩展: 控制流](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/LangImpl05.html)
- [06. Kaleidoscope: 扩展: 用户定义操作符](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/LangImpl06.html)
- [07. Kaleidoscope: 扩展: 可变变量](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/LangImpl07.html)
- [08. Kaleidoscope: 编译到目标代码](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/LangImpl08.html)
- [09. Kaleidoscope: 添加调试信息](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/LangImpl09.html)
- [10. Kaleidoscope: 总结和其他有用的LLVM小道消息](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/LangImpl10.html)

## 在LLVM中构建JIT
- [1. Building a JIT: 从 KaleidoscopeJIT 开始]
- [2. Building a JIT: 添加优化– ORC层简介]
- [3. Building a JIT: 按功能的惰性编译]
- [4. Building a JIT: 极端懒惰-Using LazyReexports to JIT from ASTs]

## 扩展教程
- [教程：为Cpu0架构创建LLVM后端](http://jonathan2251.github.com/lbd/)
    - 开发LLVM后端的分步教程。正在 https://github.com/Jonathan2251/lbd 进行积极开发（请支持！）
- [如何：实现LLVM集成汇编程序](http://www.embecosm.com/appnotes/ean10/ean10-howto-llvmas-1.0.html)
    - 关于如何为架构实现LLVM集成汇编程序的简单指南
- 高阶主题
    - [为LLVM编写优化](https://llvm.org/pubs/2004-09-22-LCPCLLVMTutorial.html)




