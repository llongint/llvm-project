# [参考](https://llvm.org/docs/Reference.html)

LLVM和API参考文档

- [API参考](#API参考)
- [LLVM参考](#LLVM参考)
    - [命令行实用程序](#命令行实用程序)
    - [资源回收](#资源回收)
    - [LibFuzzer](#LibFuzzer)
    - [LLVM IR](#IR)
    - [测试与调试](#测试与调试)
    - [XRay](#XRay)
    - [补充主题](#补充主题)

## API参考
- [doxygen生成的文档](https://llvm.org/doxygen/)
    - [类 - 继承关系](https://llvm.org/doxygen/inherits.html)
- [怎么用 attributes](https://llvm.org/docs/HowToUseAttributes.html)
    - 回答有关新的Attributes基础结构的一些问题
- [绑定的 GO 语言文档](http://godoc.org/llvm.org/llvm/bindings/go/llvm)

## LLVM参考
### 命令行实用程序
- [LLVM命令指南](https://llvm.org/docs/CommandGuide/index.html)
    - LLVM命令行实用程序的参考手册（LLVM工具的“手册”页面）
- [LLVM BUG 定位工具：设计和使用](https://llvm.org/docs/Bugpoint.html)
    - 自动发现错误和减少测试用例的描述和使用信息
- [使用 -opt-bisect-limit 调试优化错误](https://llvm.org/docs/OptBisect.html)
    - 用于调试优化引发的故障的命令行选项
- [Microsoft PDB文件格式](https://llvm.org/docs/PDB/index.html)
    - Microsoft PDB（程序数据库）文件格式的详细说明

### 资源回收
- [LLVM进行资源回收](https://llvm.org/docs/GarbageCollection.html)
    - 源语言编译器应该使用这些接口来编译GC程序
- [LLVM中的资源回收安全点](https://llvm.org/docs/Statepoints.html)
    - 这描述了一组针对垃圾收集支持的实验性扩展

### LibFuzzer
- [libFuzzer –用于覆盖率指导的模糊测试的库](https://llvm.org/docs/LibFuzzer.html)
    - 用于编写过程中指导的模糊器的库
- [模糊化LLVM库和工具](https://llvm.org/docs/FuzzingLLVM.html)
    - 有关编写和使用Fuzzers查找LLVM中的错误的信息

### IR
- [LLVM语言参考手册](LangRef.md)
    - 定义不同节点的 LLVM 中间表示和组装形式
- [InAlloca属性的设计和使用](https://llvm.org/docs/InAlloca.html)
    - inalloca参数属性的描述
- [LLVM位码文件格式](https://llvm.org/docs/BitCodeFormat.html)
    - 这描述了用于LLVM `.bc` 文件的文件格式和编码
- [机器IR(MIR)格式参考手册](https://llvm.org/docs/MIRLangRef.html)
    - MIR序列化格式的参考手册，用于测试LLVM的代码生成过程
- [全局指令选择](https://llvm.org/docs/GlobalISel/index.html)
    - 这描述了原型指令选择替换GlobalISel
### 测试与调试
- [LLVM测试基础架构指南](TestingGuide.md)
    - 使用LLVM测试基础结构的参考手册
- [测试套件指南](https://llvm.org/docs/TestSuiteGuide.html)
    - 介绍如何编译和运行测试套件基准
- [GWP-ASan](https://llvm.org/docs/GwpAsan.html)
    - A sampled heap memory error detection toolkit designed for production use.

### XRay
- XRay 介绍
    - 有关如何在LLVM中使用XRay的高级文档
- [使用XRay测试](https://llvm.org/docs/XRayExample.html)
    - 如何使用XRay调试应用程序的示例

### 补充主题
- [FaultMap和隐式检查](https://llvm.org/docs/FaultMaps.html)
    - LLVM支持将控制流折叠到故障机器指令中
- [LLVM原子指令和并发指南](https://llvm.org/docs/Atomics.html)
    - 有关LLVM并发模型的信息
- [LLVM中的异常处理](https://llvm.org/docs/ExceptionHandling.html)
    - 本文档介绍了LLVM中异常处理的设计和实现
- [LLVM扩展](https://llvm.org/docs/Extensions.html)
    - LLVM专用于工具和格式的扩展LLVM寻求与之兼容
- [如何为您的类层次结构设置LLVM样式的RTTI](https://llvm.org/docs/HowToSetUpLLVMStyleRTTI.html)
    - 如何使 `isa<>，dyn_cast<>` 等可用于您的类层次结构的客户端
- [LLVM块频率术语](https://llvm.org/docs/BlockFrequencyTerminology.html)
    - 提供有关在BlockFrequencyInfo分析过程中使用的术语的信息
- [LLVM分支权重元数据](https://llvm.org/docs/BranchWeightMetadata.html)
    - 提供有关分支预测信息的信息
- [经常被误解的GEP指令](https://llvm.org/docs/GetElementPtr.html)
    - 回答有关LLVM最常被误解的指令的一些非常常见的问题
- [伪造的硬化分配器](https://llvm.org/docs/ScudoHardenedAllocator.html)
    - 一个实现安全性增强的 malloc() 库
- [MemTagSanitizer](https://llvm.org/docs/MemTagSanitizer.html)
    - 针对生产代码的安全性增强，旨在缓解与内存相关的漏洞。基于Armv8.5-A内存标记扩展
- [依赖图](https://llvm.org/docs/DependenceGraphs/index.html)
    - 对各种依赖图（例如DDG（数据依赖图））的设计的描述
- [推测性负载硬化](https://llvm.org/docs/SpeculativeLoadHardening.html)
    - Spectre v1的“推测性负载硬化”缓解措施的说明
- [LLVM中的分段堆栈](https://llvm.org/docs/SegmentedStacks.html)
    - 本文档介绍分段堆栈及其在LLVM中的使用方式
- [LLVM的可选丰富反汇编输出](https://llvm.org/docs/MarkedUpDisassembly.html)
    - 本文档介绍了可选的丰富反汇编输出语法
- [LLVM中的堆栈图和补丁点](https://llvm.org/docs/StackMaps.html)
    - LLVM支持将指令地址映射到值的位置，并允许对代码进行修补
- [LLVM中的协程](https://llvm.org/docs/Coroutines.html)
    - LLVM对协程的支持
- [YAML I/O](https://llvm.org/docs/YamlIO.html)
    - 使用LLVM的 YAML I/O 库的参考指南



