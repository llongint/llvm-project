[TOC]

# [LLVM 官网文档](https://llvm.org/docs/)

## 关于
> 如果您使用的是LLVM的发行版，请参见 [下载页面](https://llvm.org/releases/) 以找到您的文档。

LLVM编译器基础结构支持广泛的项目，从工业强度的编译器到专业的JIT应用程序再到小型研究项目。 同样，文档也分为针对不同受众的几个高级组：

### LLVM设计与概述
几篇介绍性论文和演讲
- [LLVM编译器简介]
    - 演示为用户提供了LLVM的介绍。
- [LLVM简介]
    - 《The Architecture of Open Source Applications》一书中的一章描述了影响LLVM的高层设计决策
- [LLVM：终生程序分析和转换的编译框架]
    - 设计概述
- [LLVM：多阶段优化的基础架构]
    - 更多细节（现在已经很旧了）
### 文献资料
入门，操作方法，开发人员指南和教程
- [入门/教程](GettingStartedTutorials.md)
    - 对于LLVM系统的新手
- [用户指南]()
    - 用户指南和操作方法
- [参考](Reference.md)
    - LLVM和API参考文档

### 社区
LLVM欢迎各种贡献。要了解更多信息，请参阅以下文章
- [Getting Involved]
- [开发过程]
- [邮件列表]
- [聚会和社交活动]
- [社区范围内的提案]
    - 报告安全问题
- [如何报告安全问题?]

### 参数列表
- [参数](https://llvm.org/docs/genindex.html)
- [搜索](https://llvm.org/docs/search.html)




