
# [LLVM系统入门](https://llvm.org/docs/GettingStarted.html)


- [概述](#概述)
- [LLVM 源码获取与构建](#LLVM源码获取与构建)
- [环境要求](#环境要求)
    - [硬件](#硬件)
    - [软件](#软件)
    - [C++工具链，包括编译器和标准库](#C++工具链，包括编译器和标准库)
        - [获得最新的C++工具链](#获得最新的C++工具链)
- [LLVM入门](#LLVM入门)
    - [术语和符号](#术语和符号)
    - [解压缩LLVM存档](#解压缩LLVM存档)
    - [从Git检出LLVM](#从Git检出LLVM)
        - [发送补丁](#发送补丁)
        - [让开发人员从Git提交更改](#让开发人员从Git提交更改)
        - [Git pre-push hook](#pre-push_hook)
        - [Bisecting commits](#Bisecting提交)
        - [还原更改](#还原更改)
    - [本地 LLVM 配置](#本地LLVM配置)
    - [编译LLVM套件源代码](#编译LLVM套件源代码)
    - [交叉编译LLVM](#交叉编译LLVM)
    - [LLVM对象文件的位置](#LLVM对象文件的位置)
    - [可选配置项目](#可选配置项目)
- [目录布局](#目录布局)
    - [llvm/examples](#llvm/examples)
    - [llvm/include](#llvm/include)
    - [llvm/lib](#llvm/lib)
    - [llvm/projects](#llvm/projects)
    - [llvm/test](#llvm/test)
    - [test-suite](#test-suite)
    - [llvm/tools](#llvm/tools)
    - [llvm/utils](#llvm/utils)
- [使用LLVM工具链的示例](#使用LLVM工具链的示例)
    - [一个clang的例子](#一个clang的例子)
- [常见问题](#常见问题)
- [链接](#链接)
---
## 概述
欢迎使用 LLVM 项目!

LLVM 项目包含很多部分，核心部分跟本身一样叫 “LLVM”，它包含处理中间表示并将其转换为目标文件所需的所有工具、库、头文件。工具包括汇编程序，反汇编程序，位代码分析器和位代码优化器。它还包含基本的回归测试。

类似C的语言使用[Clang](http://clang.llvm.org/)前端。该组件将C，C ++，Objective-C和Objective-C ++代码编译为LLVM位代码-并使用LLVM从那里编译为目标文件。

其他组成部分包括：
- [libc++ C++ standard library](https://libcxx.llvm.org),
- [LLD linker](https://lld.llvm.org), 等.

## LLVM源码获取与构建
开始页的文档或许已经过时了，[clang 的入门文档](http://clang.llvm.org/get_started.html) 可能包含更新的信息。

这是一个获取和构建LLVM源的示例工作流程和配置：

1. 检出 LLVM (包括类似 clang 的关联子项目):

     * ``git clone https://github.com/llvm/llvm-project.git``

     * 或者，在windows上, ``git clone --config core.autocrlf=false
    https://github.com/llvm/llvm-project.git``

2. 设置和构建 LLVM 和 Clang:

     * ``cd llvm-project``

     * ``cmake -S llvm -B build -G <generator> [options]``

        一些常见的构建系统生成器是:

        * ``Ninja`` --- 用于生成 [Ninja](https://ninja-build.org)
          构建文件. 大多数 llvm 开发者使用 Ninja.
        * ``Unix Makefiles`` --- 用于生成与make兼容的makefile.
        * ``Visual Studio`` --- 用于生成 Visual Studio项目和解决方案.
        * ``Xcode`` --- 用于生成 Xcode 项目.

        一些常见选项:

        * ``-DLLVM_ENABLE_PROJECTS='...'`` --- 您要另外构建的LLVM子项目的列表，以分号分隔。可以包括以下任何一项：clang，clang-tools-extra，libcxx，libcxxabi，libunwind，lldb，compiler-rt，lld，poly或debuginfo-tests.

          举个例子, 要构建 LLVM, Clang, libcxx, and libcxxabi, 可以这样：
          ``-DLLVM_ENABLE_PROJECTS="clang;libcxx;libcxxabi"``.

        * ``-DCMAKE_INSTALL_PREFIX=directory`` ---指定要在其中安装LLVM工具和库的完整路径名(默认是 ``/usr/local``).

        * ``-DCMAKE_BUILD_TYPE=type`` --- 合法的选项是 Debug,
          Release, RelWithDebInfo, 和 MinSizeRel. 默认是 Debug.

        * ``-DLLVM_ENABLE_ASSERTIONS=On`` --- 在启用断言检查的情况下进行编译（对于Debug版本，默认值为Yes；对于所有其他版本类型，默认值为No）.

      * ``cmake --build build [-- [options] <target>]`` 或直接在上面指定的构建系统.

        * 默认目标 (比如. ``ninja`` or ``make``) 会构建 LLVM 的所有项目.

        * ``check-all`` 目标 (比如. ``ninja check-all``) 或直接在上面指定的构建系统.

        * CMake 将为每个工具和库生成目标，并且大多数LLVM子项目都会生成自己的 ``check-<project>`` 目标

        * 运行串行构建会 **很慢**。为了提高速度，请尝试运行并行构建。默认情况下，这是在 Ninja 中完成的；对于make，请使用选项-j NNN，其中NNN是并行作业的数量，例如您拥有的CPU数量

      * 更多信息请参见 [CMake](https://llvm.org/docs/CMake.html)
---
## 环境要求
在开始使用LLVM系统之前，请阅读以下要求。提前了解所需的硬件和软件，可以为您节省一些麻烦。
### 硬件
众所周知，LLVM可在以下主机平台上运行：

OS	|Arch	|Compilers
|:--|:--|:--|
Linux	|x861	|GCC, Clang
Linux	|amd64	|GCC, Clang
Linux	|ARM	|GCC, Clang
Linux	|Mips	|GCC, Clang
Linux	|PowerPC|GCC, Clang
Linux	|SystemZ|GCC, Clang
...     |...    |...

> 注意<br>
> 1、奔腾处理器及更高版本支持代码生成 <br>
> 2、仅32位ABI支持代码生成 <br>
> 3、要在基于Win32的系统上使用LLVM模块，可以使用-DBUILD_SHARED_LIBS = On配置LLVM<br>

- 请注意，调试版本需要大量时间和磁盘空间。仅LLVM的版本将需要大约1-3 GB的空间。完整版本的LLVM和Clang将需要大约15-20 GB的磁盘空间。确切的空间要求将因系统而异。 （由于所有调试信息，而且库被静态链接到多个工具中，因此它是如此之大）。 
- 如果空间有限，则只能构建选定的工具或选定的目标。发布版本需要的空间要少得多。 
- LLVM套件可以在其他平台上编译，但不能保证这样做。如果编译成功，则LLVM实用程序应该能够汇编，反汇编，分析和优化LLVM位代码。尽管生成的本机代码可能无法在您的平台上运行，但是代码生成也应正常工作。

### 软件
编译LLVM需要安装多个软件包。下表列出了那些必需的软件包。 Package列是LLVM依赖的软件包的常用名称。 “版本”列提供了软件包的“已知有效”版本。注释列描述了LLVM如何使用该软件包并提供了其他详细信息。

Package	|Version	|Notes
|:--|:--|:--|
CMake	|>=3.13.4	|Makefile/workspace generator
GCC	|>=5.1.0	|C/C++ compiler1
python	|>=3.6	|Automated test suite2
zlib	|>=1.2.3.4	|Compression library3
GNU Make	|3.79, 3.79.1	|Makefile/build processor4

### C++工具链，包括编译器和标准库
LLVM对主机C++编译器的要求很高，因此倾向于暴露编译器中的错误。我们还尝试合理地密切关注C ++语言和库中的改进和发展。因此，我们需要现代的宿主C ++工具链（包括编译器和标准库）来构建LLVM

LLVM是使用[编码标准]中(https://llvm.org/docs/CodingStandards.html)记录的C++子集编写的。为了实施该语言版本，我们在构建系统中检查了最受欢迎的工具链，以获取特定的最低版本:
- Clang 3.5
- Apple Clang 6.0
- GCC 5.1
- Visual Studio 2017

任何比这些工具链还早的版本都可以使用，但是将需要使用特殊选项来强制构建系统，而实际上并不是受支持的宿主平台。还要注意，这些编译器的较旧版本经常崩溃或错误地编译了LLVM。

对于不那么广泛使用的主机工具链（例如ICC或xlC），请注意，可能需要最新版本才能支持LLVM中使用的所有C++功能。

我们跟踪某些特定版本的软件，这些版本在用作主机工具链的一部分时会失败。这些甚至有时包括链接器。

`GNU ld2.16.X`: ld链接程序的某些2.16.X版本会产生很长的警告消息，提示您在废弃的部分中定义了一些“ .gnu.linkonce.t。*”符号。您可以放心地忽略这些消息，因为它们是错误的并且链接正确。这些消息在ld 2.17中消失

`GNU binutils 2.17:` ......

`GNU Binutils 2.19.1 Gold:` ......

#### 获得最新的C++工具链

本节主要适用于Linux和较旧的BSD。在macOS上，您应该具有足够现代的Xcode，否则您可能需要升级才能升级。 Windows没有“系统编译器”，因此您必须安装Visual Studio 2017或mingw64的最新版本。 FreeBSD 10.0及更高版本具有现代的Clang作为系统编译器。 

但是，某些Linux发行版和某些其他或更旧的BSD有时具有非常旧的GCC版本。这些步骤试图帮助您即使在这样的系统上也可以升级编译器。但是，如果可能的话，我们建议您将发行版的最新版本与满足这些要求的现代系统编译器一起使用。请注意，试图将Clang和libc ++的早期版本安装为主机编译器是很诱人的，但是直到最近才对libc ++进行了良好的测试或对其进行设置以在Linux上进行构建。因此，本指南建议仅使用libstdc ++和现代GCC作为引导程序中的初始主机，然后使用Clang （可能还有libc ++）。

第一步是安装最新的GCC工具链。用户难以满足版本要求的最常见发行版是Ubuntu Precise，12.04 LTS。对于此发行版，一个简单的选择是安装测试[PPA的工具链](https://launchpad.net/~ubuntu-toolchain-r/+archive/test)，并使用它来安装现代的GCC。在[ask ubuntu堆栈交换](https://askubuntu.com/questions/466651/how-do-i-use-the-latest-gcc-on-ubuntu/581497#58149)和带有更新命令的 [github gist](https://gist.github.com/application2000/73fd6f4bf1be6600a2cf9f56315a2d91) 上对此进行了非常好的讨论。但是，并非所有用户都可以使用PPA，并且还有许多其他发行版，因此从源代码构建和安装GCC可能是必要的（或者很有用，如果您在这里，您正在进行编译器开发）。如今做起来也很容易。

安装GCC 5.1.0的简单步骤：
```{.bash}
% gcc_version=5.1.0
% wget https://ftp.gnu.org/gnu/gcc/gcc-${gcc_version}/gcc-${gcc_version}.tar.bz2
% wget https://ftp.gnu.org/gnu/gcc/gcc-${gcc_version}/gcc-${gcc_version}.tar.bz2.sig
% wget https://ftp.gnu.org/gnu/gnu-keyring.gpg
% signature_invalid=`gpg --verify --no-default-keyring --keyring ./gnu-keyring.gpg gcc-${gcc_version}.tar.bz2.sig`
% if [ $signature_invalid ]; then echo "Invalid signature" ; exit 1 ; fi
% tar -xvjf gcc-${gcc_version}.tar.bz2
% cd gcc-${gcc_version}
% ./contrib/download_prerequisites
% cd ..
% mkdir gcc-${gcc_version}-build
% cd gcc-${gcc_version}-build
% $PWD/../gcc-${gcc_version}/configure --prefix=$HOME/toolchains --enable-languages=c,c++
% make -j$(nproc)
% make install
```
有关更多详细信息，请查看出色的[GCC Wiki](https://gcc.gnu.org/wiki/InstallingGCC)条目，我从中获得了大部分此类信息。

## LLVM入门
本指南的其余部分旨在帮助您开始使用LLVM，并为您提供有关LLVM环境的一些基本信息。 

本指南后面的部分介绍了LLVM源树的一般布局，使用LLVM工具链的简单示例，以及用于查找有关LLVM的更多信息或通过电子邮件获得帮助的链接。

### 术语和符号
在本手册中，以下名称用于表示特定于本地系统和工作环境的路径。这些不是您需要设置的环境变量，而只是下面本文其余部分中使用的字符串。在下面的任何示例中，只需在本地系统上用适当的路径名替换这些名称中的每一个即可。所有这些路径都是绝对的：

- SRC_ROOT
    - 这是LLVM源树的顶级目录
- OBJ_ROOT
    - 这是LLVM对象树的顶层目录（即将放置目标文件和编译程序的树。它可以与SRC_ROOT相同）
### 解压缩LLVM存档
如果您拥有LLVM发行版，则需要先解压缩它，然后才能开始对其进行编译。 LLVM分布为许多不同的子项目。每个文件都有自己的下载文件，该文件是TAR归档文件，已使用gzip程序压缩。
- llvm-x.y.tar.gz
    - LLVM库和工具的源代码发行版
- cfe-x.y.tar.gz
    - Clang前端的源代码发布

### 从Git检出LLVM
```{.bash}
$ git clone https://github.com/llvm/llvm-project.git
$ # or on windows 
$ git clone --config core.autocrlf=false https://github.com/llvm/llvm-project.git
```
这将在当前目录中创建一个“ llvm-project”目录，并使用LLVM和所有相关子项目的所有源代码，测试目录以及文档文件的本地副本完全填充该目录。请注意，与将每个子项目包含在一个单独文件中的tarball不同，git存储库将所有项目都包含在一起。 

如果要获取特定版本（而不是最新版本），则可以在克隆存储库后检出标签。例如，由上述命令创建的 llvm-project 目录中的`git checkout llvmorg-6.0.1`。使用`git tag -l`列出所有它们

#### 发送补丁
- 请参考 [开发协议](https://llvm.org/docs/DeveloperPolicy.html#one-off-patches)
- 我们目前不接受github pull请求，因此您需要通过电子邮件发送给llvm-commits或最好通过 [Phabricator](https://llvm.org/docs/Phabricator.html#phabricator-reviews) 发送补丁
- 虽然Phabricator是某些工具的有用工具，但相关的-commits邮件列表是所有LLVM代码检查的记录系统。邮件列表应作为订阅者添加到所有评论中，并且Phabricator用户应准备好响应发送到提交列表的邮件中的自由格式评论
- 但是，使用 [`Arcanist`](https://secure.phabricator.com/book/phabricator/article/arcanist_quick_start/) 工具通常会更容易。安装arcanist之后，您可以使用以下命令上传最新的提交：
    ```{.bash}
    % git clang-format HEAD~1
    ```
- 请注意，这会修改文件，但不会提交文件-您可能需要运行
    ```{.bash}
    % git commit --amend -a
    ```
    为了使用所有未完成的更改来更新最后的提交

#### 让开发人员从Git提交更改
一旦 patch 审查完毕，您应该 rebase，在本地重新测试，并将所做的更改 commit 给LLVM的主分支。如果您具有所需的访问权限，则可以使用git push完成此操作。请参阅为基于Phabricator的[提交更改](https://llvm.org/docs/Phabricator.html#committing-a-change)，或为提交访问获取提交访问。

- 这是使用git的示例工作流程。此工作流程假定您在名为branch-with-change的分支上具有可接受的提交:
    ```{.bash}
    # Go to the branch with your accepted commit.
    % git checkout branch-with-change
    # Rebase your change onto the latest commits on Github.
    % git pull --rebase origin main
    # Rerun the appropriate tests if needed.
    % ninja check-$whatever
    # Check that the list of commits about to be pushed is correct.
    % git log origin/main...HEAD --oneline
    # Push to Github.
    % git push origin HEAD:main
    ```
LLVM当前具有线性历史记录策略，这意味着不允许合并提交。 github上的llvm-project repo配置为拒绝包含合并的推送，因此需要上面的git rebase步骤。 

如果您在使用特定的git工作流程时遇到问题，请寻求帮助。

#### pre-push_hook
我们包括一个 pre-push hook，该 hook 将对您要推送的修订进行一些完整性检查，并询问您是否一次推送多个提交。您可以通过从存储库根目录运行来设置它（在Unix系统上）：
```{.bash}
% ln -sf ../../llvm/utils/git/pre-push.py .git/hooks/pre-push
```

#### Bisecting提交
有关如何在LLVM上使用git bisect的信息，请参阅 [Bisecting LLVM代码](https://llvm.org/docs/GitBisecting.html)

#### 还原更改
使用git还原更改时，默认消息将显示“此还原XYZ提交”。将其保留在提交消息的末尾，但在其之前添加一些有关还原原因的详细信息。简短的说明和/或指向演示该问题的漫游器的链接就足够了。

### 本地LLVM配置

检出存储库后，必须在构建LLVM套件源代码之前对其进行配置。此过程使用CMake。不同于常规的 configure 脚本，CMake会以您请求的任何格式生成构建文件以及各种 *.inc 和 llvm/include/Config/config.h

变量使用 `-D<variable name>=<value>` 传递到命令行上的cmake。以下变量是开发LLVM的人员常用的一些选项。

变量|目的
|:--|:--|
CMAKE_C_COMPILER|指定C编译器，默认是 /usr/bin/cc
CMAKE_CXX_COMPILER|指定C编译器，默认是 /usr/bin/c++
CMAKE_BUILD_TYPE|指定构建类型,有效选项是 Debug，Release，RelWithDebInfo、MinSizeRel。默认为 Debug
CMAKE_INSTALL_PREFIX|指定安装目录
PYTHON_EXECUTABLE|强制CMake使用特定的Python版本
LLVM_TARGETS_TO_BUILD|以分号分隔的列表，用于控制将构建哪些目标并将其链接到llvm中。默认列表定义为LLVM_ALL_TARGETS，并且可以设置为包括树外目标。默认值包括：AArch64, AMDGPU, ARM, BPF, Hexagon, Mips, MSP430, NVPTX, PowerPC, Sparc, SystemZ, X86, XCore
LLVM_ENABLE_DOXYGEN|从源代码构建基于doxygen的文档默认情况下，此功能处于禁用状态，因为它运行缓慢且生成大量输出
LLVM_ENABLE_PROJECTS|以分号分隔的列表，用于选择要另外构建的其他LLVM子项目。 （仅在使用并行项目布局时有效，例如通过git）。默认列表为空。可以包括：clang，libcxx，libcxxabi，libunwind，lldb，compiler-rt，lld，poly或debuginfo-tests
LLVM_ENABLE_SPHINX|从源代码构建基于sphinx的文档。默认情况下禁用此选项，因为它速度慢并且会生成大量输出。推荐使用Sphinx 1.5或更高版本
LLVM_BUILD_LLVM_DYLIB|生成libLLVM.so。该库包含一组默认的LLVM组件，可以用LLVM_DYLIB_COMPONENTS覆盖它们。默认包含大多数LLVM，并且在 tools/llvm-shlib/CMakelists.txt 中定义。该选项在Windows上不可用
LLVM_OPTIMIZED_TABLEGEN|构建在LLVM构建过程中使用的发布版本的 tablegen。这可以大大加快调试速度

可用下面两个步骤构建：
```{.sh}
% cd OBJ_ROOT
% cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=/install/path
  [other options] SRC_ROOT
```

### 编译LLVM套件源代码
与自动工具不同，使用CMake时，您的构建类型是在配置时定义的。如果要更改构建类型，可以使用以下调用重新运行cmake：
```{.sh}
% cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=type SRC_ROOT
```
在运行之间，CMake保留为所有选项设置的值。 CMake定义了以下构建类型：
- Debug
    - 这些版本是默认版本。构建系统将使用调试信息编译未声明的工具和库，并启用断言。
- Release
    - 对于这些构建，构建系统将在启用优化的情况下编译工具和库，而不生成调试信息。 CMakes的默认优化级别是-O3。这可以通过在CMake命令行上设置CMAKE_CXX_FLAGS_RELEASE变量来配置。
- RelWithDebInfo
    - 这些版本在调试时很有用。它们生成带有调试信息的优化二进制文件。 CMakes的默认优化级别是-O2。这可以通过在CMake命令行上设置CMAKE_CXX_FLAGS_RELWITHDEBINFO变量来配置

一旦配置了LLVM，就可以通过输入OBJ_ROOT目录并发出以下命令来构建它：
```{.sh}
% make
```
 如果构建失败，请在[此处检查](#C++工具链，包括编译器和标准库)是否使用了已知无法编译LLVM的GCC版本

如果您的计算机中有多个处理器，则可能希望使用GNU Make提供的一些并行构建选项。例如，您可以使用以下命令：
```{.sh}
% make -j2
```
在使用LLVM源代码时，有几个特殊的目标很有用：
- make clean
    - 删除由构建生成的所有文件。这包括目标文件，生成的C / C ++文件，库和可执行文件
- make install
    - 在CMAKE_INSTALL_PREFIX指定的$ PREFIX下的层次结构中安装LLVM头文件，库，工具和文档，该文件默认为 /usr/local
- make docs-llvm-html
    - 如果使用 `-DLLVM_ENABLE_SPHINX = On` 进行配置，这将在 OBJ_ROOT/docs/html 处生成一个目录，其中包含HTML格式的文档。

### 交叉编译LLVM
可以交叉编译LLVM本身。也就是说，您可以创建LLVM可执行文件和库，以将其托管在与构建它们的平台不同的平台上（Canadian 跨平台构建）。为了生成用于交叉编译的构建文件，CMake提供了一个变量 `CMAKE_TOOLCHAIN_FILE`，该变量可以定义在CMake测试操作期间使用的编译器标志和变量

这种构建的结果是可执行文件，这些可执行文件无法在构建主机上运行，​​但可以在目标计算机上执行。例如，以下CMake调用可以生成针对iOS的构建文件。这将在具有最新Xcode的macOS上运行：
```{.sh}
% cmake -G "Ninja" -DCMAKE_OSX_ARCHITECTURES="armv7;armv7s;arm64"
  -DCMAKE_TOOLCHAIN_FILE=<PATH_TO_LLVM>/cmake/platforms/iOS.cmake
  -DCMAKE_BUILD_TYPE=Release -DLLVM_BUILD_RUNTIME=Off -DLLVM_INCLUDE_TESTS=Off
  -DLLVM_INCLUDE_EXAMPLES=Off -DLLVM_ENABLE_BACKTRACES=Off [options]
  <PATH_TO_LLVM>
```
注意：由于iOS SDK的限制，在为iOS构建时还需要传递一些其他标志

有关如何交叉编译的更多信息，请参阅如何使用 [Clang/LLVM 交叉编译Clang/LLVM](https://llvm.org/docs/HowToCrossCompileLLVM.html) 和 [Clang文档](https://clang.llvm.org/docs/CrossCompilation.html) 了解如何，了解有关如何交叉编译的一般信息。

### LLVM对象文件的位置
LLVM构建系统能够在多个LLVM构建之间共享单个LLVM源树。因此，可以使用同一源代码树为几种不同的平台或配置构建LLVM

将目录更改为LLVM对象文件应驻留的位置 并运行 cmake
```{.sh}
% cd OBJ_ROOT
% cmake -G "Unix Makefiles" SRC_ROOT
```
LLVM构建将在OBJ_ROOT下创建一个与LLVM源树匹配的结构。在源代码树中存在源文件的每个级别上，在OBJ_ROOT中将有一个对应的CMakeFiles目录。在该目录下，还有另一个名称以.dir结尾的目录，在该目录下，您将找到每个源的目标文件

比如：
```{.sh}
% cd llvm_build_dir
% find lib/Support/ -name APFloat*
lib/Support/CMakeFiles/LLVMSupport.dir/APFloat.cpp.o
```

### 可选配置项目
如果您在支持 [binfmt_misc](http://en.wikipedia.org/wiki/binfmt_misc) 模块的Linux系统上运行，并且对系统具有root访问权限，则可以将系统设置为直接执行LLVM位代码文件。为此，请使用以下命令（如果您已经在使用该模块，则可能不需要第一个命令）：
```{.sh}
% mount -t binfmt_misc none /proc/sys/fs/binfmt_misc
% echo ':llvm:M::BC::/path/to/lli:' > /proc/sys/fs/binfmt_misc/register
% chmod u+x hello.bc   (if needed)
% ./hello.bc
```
这使您可以直接执行LLVM位代码文件。在Debian上，您也可以使用以下命令代替上面的“ echo”命令：
```{.sh}
% sudo update-binfmts --install llvm /path/to/lli --magic 'BC'
```

## 目录布局
关于LLVM源码库的有用信息之一是LLVM [doxygen](http://www.doxygen.org/) 文档，该文档可从 [https://llvm.org/doxygen/](https://llvm.org/doxygen/) 获得。以下是代码布局的简要介绍

### llvm/examples
使用LLVM IR和JIT的简单示例。
### llvm/include
从LLVM库导出的公共头文件。三个主要子目录：
- llvm/include/llvm
    - 所有LLVM特定的头文件，以及LLVM的不同部分的子目录：Analysis，CodeGen，Target，Transforms等…
- llvm/include/llvm/Support
    - LLVM附带的通用支持库，但不一定特定于LLVM。例如，某些 C++ STL 实用程序和命令行选项处理库在此处存储头文件。
- llvm/include/llvm/Config
    - 由cmake配置的头文件。它们包装“标准” UNIX和C头文件。源代码可以包括这些头文件，这些头文件会自动处理cmake生成的条件#include
### llvm/lib
大多数源文件在这里。通过将代码放入库中，LLVM使得在工具之间共享代码变得容易
- llvm/lib/IR/
    - 实现诸如Instruction和BasicBlock之类的核心类的核心LLVM源文件
- llvm/lib/AsmParser/
    - LLVM汇编语言解析器库的源代码
- llvm/lib/Bitcode/
    - 用于读取和写入位码的代码
- llvm/lib/Analysis/
    - 各种程序分析，例如调用图，归纳变量，自然循环标识等
- llvm/lib/Transforms/
    - IR到IR程序的转换，例如积极的死代码消除，稀疏的条件常数传播，内联，循环不变代码运动，死全局消除等
- llvm/lib/Target/
    - 描述用于代码生成的目标体系结构的文件。例如，llvm/lib/Target/X86 保存X86计算机的描述
- llvm/lib/CodeGen/
    - 代码生成器的主要部分：指令选择器，指令调度和寄存器分配
- llvm/lib/MC/
    - (FIXME: T.B.D.) ….?
- llvm/lib/ExecutionEngine/
    - 用于在解释的和JIT编译的场景中在运行时直接执行位代码的库
- llvm/lib/Support/
    - 与 llvm/include/ADT/ 和 llvm/include/Support/ 中的头文件相对应的源代码

### llvm/projects
项目不是严格地属于LLVM的一部分，而是与LLVM一起提供的。这也是用于创建自己的基于LLVM的项目的目录，该项目利用LLVM构建系统

### llvm/test
LLVM基础架构上的功能和回归测试以及其他完整性检查。它们旨在快速运行并覆盖很多领域，而并非详尽无遗。

### test-suite
LLVM的全面正确性，性能和基准测试套件。这位于单独的git存储库中，因为在各种许可下，它包含大量的第三方代码。有关详细信息，请参见 [《测试指南》](https://llvm.org/docs/TestingGuide.html) 文档。

### llvm/tools
在上述库的基础上构建的可执行文件，它们构成用户界面的主要部分。您始终可以通过输入tool_name -help获得有关工具的帮助。以下是最重要的工具的简要介绍。更详细的信息在 [命令指南](https://llvm.org/docs/CommandGuide/index.html)
- bugpoint
    - 通过将给定的测试用例缩小到最少数量的通过 或 仍然导致问题（无论是崩溃还是错误的编译）的指令，bugpoint用于调试优化过程或代码生成后端。有关使用错误点的更多信息，请参见 [HowToSubmitABug.html](https://llvm.org/docs/HowToSubmitABug.html)
- llvm-ar
    - 存档器生成包含给定LLVM位代码文件的存档，还可以选择包含索引的索引，以加快查找速度
- llvm-as
    - 汇编程序将人类可读的LLVM程序集转换为LLVM位代码
- llvm-dis
    - 反汇编程序将LLVM位代码转换为人类可读的LLVM程序集
- llvm-link
    - 毫不奇怪，llvm-link将多个LLVM模块链接到一个程序中
- lli
    - lli是LLVM解释器，可以直接执行LLVM位代码（尽管非常慢……）。对于支持它的体系结构（当前为x86，Sparc和PowerPC），默认情况下，lli将充当即时编译器（如果功能已在其中编译），并且执行代码的速度将比解释器快得多
- llc
    - llc是LLVM后端编译器，它将LLVM位代码转换为本地代码汇编文件
- opt
    - opt读取LLVM位代码，将一系列LLVM应用于LLVM转换（在命令行上指定），然后输出结果位代码。 “ opt -help”是获取LLVM中可用程序转换列表的好方法
    - opt还可以对输入的LLVM位代码文件运行特定的分析并打印结果。主要用于调试分析或熟悉分析功能

### llvm/utils
用于处理LLVM源代码的实用程序；有些是构建过程的一部分，因为它们是基础结构各部分的代码生成器
- codegen-diff
    - `codegen-diff` 查找LLC生成的代码与LLI生成的代码之间的差异。如果您要调试其中一个，并且假定另一个生成正确的输出，这将很有用。要获得完整的用户手册，请运行 `perldoc codegen-diff`
- emacs/
    - LLVM汇编文件和TableGen描述文件的Emacs和XEmacs语法突出显示。有关使用它们的信息，请参见`README`
- getsrcs.sh
    - 查找并输出所有未生成的源文件，如果希望跨目录进行大量开发并且不想查找每个文件，则很有用。一种使用它的方法是运行，例如：从LLVM源代码树的顶部开始xemacs`utils / getsources.sh`。
- llvmgrep
    - 在LLVM中的每个源文件上执行 `egrep -H -n`，并将 llvmgrep 命令行中提供的正则表达式传递给它。这是在源库中搜索特定正则表达式的有效方法
- TableGen/
    - 包含用于从通用 TableGen 描述文件生成寄存器描述，指令集描述甚至汇编器的工具
- vim/
    - vim语法突出显示LLVM汇编文件和TableGen描述文件。请参阅 `README` 以了解如何使用它们

## 使用LLVM工具链的示例
本节提供了将LLVM与Clang前端一起使用的示例
### 一个clang的例子
- 1.创建一个简单的 hello.c
    ```c
    #include <stdio.h>
    int main() {
    printf("hello world\n");
    return 0;
    }
    ```
- 2.接下来，将C文件编译为本地可执行文件
    ```sh
    % clang hello.c -o hello
    ```
> Note<br>
> 默认情况下 Clang 用起来就像 GCC 一样. 标准参数 -S 和 -c 作用与往常无异 (生成一个 native .s or .o file, respectively).

- 3.接下来，将C文件编译为LLVM位代码文件:
    ```sh
    % clang -O3 -emit-llvm hello.c -c -o hello.bc
    ```
    - -emit-llvm选项可以与-S或-c选项一起使用，以分别为代码发出LLVM .ll或.bc文件。这使您可以在位码文件上使用标准的 [LLVM工具](https://llvm.org/docs/CommandGuide/index.html) 

- 4.要运行两种形式运行程序，请使用：
    ```sh
    % ./hello
    % lli hello.bc
    ```
    - 第二个示例显示了如何调用LLVM JIT, lli.

- 5.使用llvm-dis实用程序查看LLVM汇编代码:
    ```
    % llvm-dis < hello.bc | less
    ```
- 6.使用LLC代码生成器将程序编译为本机程序集:
    ```
    % llc hello.bc -o hello.s
    ```
- 7.将本机汇编语言文件组装到程序中:
    ```
    % /opt/SUNWspro/bin/cc -xarch=v9 hello.s -o hello.native   # On Solaris
    % gcc hello.s -o hello.native                              # On others, gcc/clang
    ```
- 8.执行本机代码程序:
    ```
    % ./hello.native
    ```
    - 请注意，使用clang直接编译为本机代码（即-emit-llvm选项不存在时）会为您执行步骤6/7/8.


## 常见问题
如果您在构建或使用LLVM时遇到问题，或者对LLVM有任何其他一般性问题，请查阅 [常见问题](https://llvm.org/docs/FAQ.html) 页面

如果您在有限的内存和构建时间上遇到问题，请尝试使用 Ninja 而不是 make 进行构建。请考虑使用cmake配置以下选项：
- `-G Ninja` 设置此选项将使您可以使用ninja而不是make进行构建。使用忍者进行构建可以显着缩短构建时间（尤其是增量构建），并可以提高内存使用率。
- `-DLLVM_USE_LINKER` 将此选项设置为lld将大大减少基于ELF的平台（例如Linux）上LLVM可执行文件的链接时间。如果您是第一次构建LLVM，并且lld无法作为二进制软件包使用，那么您可能希望使用 gold linker 作为 GNU ld 的更快替代方案
- `-DCMAKE_BUILD_TYPE`
    - Debug : 这是默认的构建类型。这会在编译LLVM时禁用优化，并启用调试信息。在基于ELF的平台（例如Linux）上，链接调试信息可能会占用大量内存
    - Release : 打开优化并禁用调试信息。将Release构建类型与 `-DLLVM_ENABLE_ASSERTIONS = ON` 结合使用可能会在开发过程中的速度和可调试性之间取得良好的平衡，尤其是对于运行测试套件而言
- `-DLLVM_ENABLE_ASSERTIONS` 对于Debug版本，此选项默认为ON；对于Release版本，此选项默认为OFF。如上一个选项所述，使用Release版本类型并启用断言可能是使用Debug版本类型的不错选择
- `-DLLVM_OPTIMIZED_TABLEGEN` 将此设置为 ON 可以在构建过程中生成完全优化的tablegen。这将大大缩短您的构建时间。仅在使用 `debug` 构建类型时才有用
- `-DLLVM_ENABLE_PROJECTS` 将此值设置为等于要编译的项目（例如clang，lld等）。如果编译多个项目，请用分号分隔各个项目。如果遇到分号问题，请尝试用单引号将其引起来
- `-DCLANG_ENABLE_STATIC_ANALYZER` 如果不需要clang静态分析器，则将此选项设置为OFF。这应该会稍微缩短您的构建时间
- `-DLLVM_USE_SPLIT_DWARF` 如果需要调试版本，请考虑将其设置为ON，因为这将减轻链接器上的内存压力。这将使链接更快，因为二进制文件将不包含任何调试信息。但是，这将以DWARF目标文件（扩展名为.dwo）的形式生成调试信息。这仅适用于使用ELF的主机平台，例如Linux

## 链接
本文档只是有关如何使用LLVM来完成一些简单操作的介绍……还有很多有趣而复杂的操作可以在此处进行记录（但是如果您想编写一些内容，我们很乐意接受补丁！）。有关LLVM的更多信息，请查看：
- [LLVM主页](https://llvm.org/)
- [LLVM Doxygen Tree](https://llvm.org/doxygen/)
- [使用LLVM的开启一个项目](https://llvm.org/docs/Projects.html)


