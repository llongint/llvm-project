
# [入门/教程](https://llvm.org/docs/GettingStartedTutorials.html)

写给 LLVM 系统的新手

- [LLVM系统入门](GettingStarted.md)
    - 讨论如何使用LLVM基础架构快速启动和运行。从发行版的打包和编译到某些工具的执行，应有尽有

- [LLVM教程：目录](tutorial/index.md)
    - 有关使用LLVM的教程。包括有关使用LLVM制作自定义语言的教程

- [LLVM程序员手册]
    - 介绍LLVM源库的一般布局，重要的类和API，以及一些提示和技巧。

- [前端作者的性能提示]
    - 给前端作者的技巧提示，其中包括LLVM能够有效优化的IR生成方法。
- [使用Microsoft Visual Studio的LLVM系统入门](https://llvm.org/docs/GettingStartedVS.html)
    - 适用于Windows上使用Visual Studio的用户的主要入门指南的附录。
- [编译器作家的体系结构和平台信息](https://llvm.org/docs/CompilerWriterInfo.html)
    - 对编译器编写者有用的链接列表

