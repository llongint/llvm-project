
# [LLVM测试基础架构指南](https://llvm.org/docs/TestingGuide.html)

- [概述](#概述)
- [要求](#要求)
- [LLVM 测试基础架构组织](#LLVM测试基础架构组织)
    - [单元测试](#单元测试)
    - [回归测试](#回归测试)
    - [测试分析](#测试分析)
    - [测试套件](#测试套件)
    - [调试信息测试](#调试信息测试)
- [快速开始](#快速开始)
    - [单元测试和回归测试](#单元测试和回归测试)
    - [调试信息测试](#调试信息测试)
- [回归测试结构](#回归测试结构)
    - [编写新的回归测试](#编写新的回归测试)
    - [其他文件](#其他文件)
    - [易碎测试](#易碎测试)
    - [特定平台的测试](#特定平台的测试)
    - [限制测试执行](#限制测试执行)
    - [替换](#替换)
    - [选项](#选项)
    - [其他特性](#其他特性)


## 概述
本文档是LLVM测试基础结构的参考手册。它记录了LLVM测试基础结构的结构，使用它所需的工具以及如何添加和运行测试


## 要求

为了使用LLVM测试基础架构，您将需要构建LLVM所需的所有软件以及 [Python 3.6](http://python.org/) 或更高版本


## LLVM测试基础架构组织

LLVM测试基础结构包含三大类测试：单元测试，回归测试和整个程序。单元测试和回归测试分别包含在LLVM存储库中，位于 llvm/unittests 和 llvm/test下，并有望始终通过-它们应在每次提交之前运行

整个程序测试称为 `LLVM测试套件`（或“test-suite”），位于子版本的测试套件模块中。出于历史原因，这些测试在某些地方也称为“夜间测试”，它比“测试套件”不那么模棱两可，并且仍在使用，尽管我们比每晚运行更多次



### 单元测试

单元测试使用Google Test和Google Mock编写，位于llvm / unittests目录中。通常，保留单元测试用于支持库和其他通用数据结构，我们更喜欢依靠回归测试来测试IR上的转换和分析


### 回归测试

回归测试是一小段代码，用于测试LLVM的特定功能或触发LLVM中的特定错误。它们所使用的语言取决于要测试的LLVM的部分。这些测试由 [Lit](https://llvm.org/docs/CommandGuide/lit.html) 测试工具（它是LLVM的一部分）驱动，位于 `llvm/test` 目录中

通常，当在LLVM中发现错误时，应编写包含仅足以重现问题的代码的回归测试，并将其放置在此目录下的某个位置。例如，它可以是从实际应用程序或基准中提取的一小部分LLVM IR

### 测试分析


分析是一种推断，可以推断 IR 的某些部分的属性而不会对其进行转换。通常使用与回归测试相同的基础结构来测试它们，方法是创建单独的“打印机”通道以使用分析结果，并将其以适合 `FileCheck` 的文本格式打印在标准输出上。

有关此类测试的示例，请参见 [llvm/test/Analysis/BranchProbabilityInfo/loop.ll](https://github.com/llvm/llvm-project/blob/main/llvm/test/Analysis/BranchProbabilityInfo/loop.ll)


### 测试套件

测试套件包含整个程序，这些程序是可以编译并链接到可以执行的独立程序中的代码段。这些程序通常用高级语言（例如C或C ++）编写

这些程序使用用户指定的编译器和标志集进行编译，然后执行以捕获程序输出和时序信息。将这些程序的输出与参考输出进行比较，以确保正确编译了程序

除了编译和执行程序外，整个程序测试还可以作为基准测试 LLVM 性能的一种方式，无论是从所生成程序的效率以及LLVM编译，优化和生成代码的速度方面而言

测试套件位于测试套件子版本模块中

有关详细信息，请参见 [测试套件指南](https://llvm.org/docs/TestSuiteGuide.html)

### 调试信息测试

测试套件包含用于检查调试信息质量的测试。该测试以基于C的语言或LLVM汇编语言编写

这些测试被编译并在调试器下运行。检查调试器输出以验证调试信息。有关更多信息，请参见测试套件中的README.txt。该测试套件位于 debuginfo-tests 子版本模块中


## 快速开始

测试位于两个单独的子版本模块中。单元测试和回归测试位于主 `llvm` 模块中的 `llvm/unittests` 和 `llvm/test` 目录下（因此，您可以通过主LLVM树免费获得这些测试）。构建LLVM后，使用make check-all运行单元测试和回归测试。

测试套件模块包含更全面的测试，包括整个C和C ++程序。有关详细信息，请参见 [测试套件指南](https://llvm.org/docs/TestSuiteGuide.html)

### 单元测试和回归测试
要运行所有LLVM单元测试，请使用check-llvm-unit目标:
```sh
% make check-llvm-unit
```

要运行所有LLVM回归测试，请使用check-llvm目标：
```sh
% make check-llvm
```

为了获得合理的测试性能，请在发布模式下构建LLVM和子项目，即
```sh
% cmake -DCMAKE_BUILD_TYPE="Release" -DLLVM_ENABLE_ASSERTIONS=On
```

如果已经迁出并构建了Clang，则可以使用以下命令同时运行LLVM和Clang测试：
```sh
% make check-all
```

要使用Valgrind（默认为Memcheck）运行测试，请使用LIT_ARGS make变量将所需的选项传递给light。例如，您可以使用:
```sh
% make check LIT_ARGS="-v --vg --vg-leak"
```

以启用使用 valgrind 的测试并启用泄漏检查。<br> 
要运行单个测试或测试子集，您可以使用LLVM内置的llvm-lit脚本。例如，要单独运行Integer / BitPacked.ll测试，可以运行：
```sh
% llvm-lit ~/llvm/test/Integer/BitPacked.ll
```

或运行所有ARM CodeGen测试：
```sh
% llvm-lit ~/llvm/test/CodeGen/ARM
```

有关使用 `lit` 工具的更多信息，请参见 `llvm-lit --help` 或 [lit手册页](https://llvm.org/docs/CommandGuide/lit.html)


### 调试信息测试

要运行调试信息测试，只需将 debuginfo-tests 项目添加到 cmake 命令行上的 `LLVM_ENABLE_PROJECTS` 定义中

## 回归测试结构

LLVM回归测试由 `lit` 驱动，位于 llvm/test 目录中。<br> 
该目录包含大量的小型测试，这些测试使用 LLVM 的各种功能并确保不会发生回归。该目录分为几个子目录，每个子目录集中在LLVM的特定区域。

### 编写新的回归测试

回归测试结构非常简单，但是确实需要设置一些信息。此信息通过 cmake 收集，并写入到构建目录中的文件 `test/lit.site.cfg`。 `llvm/test` Makefile 为您完成这项工作

为了使回归测试起作用，每个测试目录必须具有 `lit.local.cfg` 文件。lit 寻找该文件，以确定如何运行测试。该文件只是Python代码，因此非常灵活，但是我们已将其标准化以用于 LLVM 回归测试。如果要添加测试目录，只需从另一个目录复制 lit.local.cfg 即可开始运行。标准的 lit.local.cfg 仅指定要检查的文件。仅包含目录的任何目录都不需要 lit.local.cfg 文件。阅读 [Lit文档](https://llvm.org/docs/CommandGuide/lit.html) 以获取更多信息。

每个测试文件必须包含以 `RUN` 开头的行，以告诉 lit 如何运行它。如果没有RUN（运行）线，则在运行测试时 lit 将发出错误消息。

在测试程序的注释中，使用关键字 RUN 加上冒号，最后是要执行的命令（管道），在测试程序的注释中指定 RUN 行。这些行一起构成了 lit 执行测试用例的“脚本”。 RUN 行的语法类似于 Shell 的管道语法，包括 I/O 重定向和变量替换。但是，即使这些行看起来像 Shell 脚本，也并非如此。 RUN线通过 lit 来解释。因此，语法在一些方面与Shell有所不同。您可以根据需要指定任意数量的 RUN 行

lit 在每个 RUN 行上执行替换，以使用为每个工具构建的可执行文件的完整路径替换 LLVM 工具名称（在 `$(LLVM_OBJ_ROOT)/$(BuildMode)/bin` 中）。这样可以确保在测试过程中，lit 不会在用户路径中调用任何杂散的LLVM工具

除非其最后一个字符为 `\`，否则每条 RUN 行均独立执行，与其他行不同。此连续字符使 RUN 行与下一行连接。这样，您可以建立较长的命令流水线，而无需增加行长。连接以 \ 结尾的行，直到找到未以 \ 结尾的 RUN 行。然后，这组连接的 RUN 行构成一个执行。 lit 将替换变量并安排要执行的管道。如果管道中的任何进程失败，那么整个生产线（和测试用例）也会失败。

以下是.ll文件中合法RUN行的示例:
```ll
; RUN: llvm-as < %s | llvm-dis > %t1
; RUN: llvm-dis < %s.bc-13 > %t2
; RUN: diff %t1 %t2
```
与 Unix shell 一样，RUN 行允许使用管道和 I/O 重定向

编写 RUN 行时，必须注意一些引用规则。通常，不需要引用任何内容。lit 不会删除任何引号字符，因此它们将被传递给调用的程序。为了避免这种情况，请使用 `花括号` 将其括起来的所有内容都视为一个值。

通常，您应该努力使 RUN 行尽可能简单，仅将它们用于运行生成文本输出然后可以检查的工具。推荐的检查输出以确定是否通过测试的方法是使用 `FileCheck` 工具。 [不建议在RUN行中使用grep-请不要发送或提交使用它的补丁。]

将相关测试放到一个文件中，而不是每个测试都放一个单独的文件。检查是否已经有文件覆盖您的功能，并考虑在其中添加代码，而不是创建一个新文件

### 其他文件

如果您的测试除了包含 RUN：的文件之外还需要其他文件，并且这些额外文件很小，请考虑在同一文件中指定它们，并使用拆分文件来提取它们。例如：
```c
; RUN: split-file %s %t
; RUN: llvm-link -S %t/a.ll %t/b.ll | FileCheck %s

; CHECK: ...

;--- a.ll
...
;--- b.ll
...
```

这些部分由正则表达式 `^(.| //)---<part>` 分隔。默认情况下，提取的内容前导空行以保留行号。指定 `--no-leading-lines` 来删除引导线。

如果多余的文件很大，则放置它们的惯用位置在子目录“输入”中。然后，您可以将额外的文件称为 %S/Inputs/foo.bar

例如，考虑 test/Linker/ident.ll。目录结构如下：
```sh
test/
  Linker/
    ident.ll
    Inputs/
      ident.a.ll
      ident.b.ll
```
为了方便起见，这些是内容:
```css
;;;;; ident.ll:

; RUN: llvm-link %S/Inputs/ident.a.ll %S/Inputs/ident.b.ll -S | FileCheck %s

; Verify that multiple input llvm.ident metadata are linked together.

; CHECK-DAG: !llvm.ident = !{!0, !1, !2}
; CHECK-DAG: "Compiler V1"
; CHECK-DAG: "Compiler V2"
; CHECK-DAG: "Compiler V3"

;;;;; Inputs/ident.a.ll:

!llvm.ident = !{!0, !1}
!0 = metadata !{metadata !"Compiler V1"}
!1 = metadata !{metadata !"Compiler V2"}

;;;;; Inputs/ident.b.ll:

!llvm.ident = !{!0}
!0 = metadata !{metadata !"Compiler V3"}
```
出于对称性原因， ident.ll只是一个虚拟文件，除了持有RUN：行之外，它实际上并未参与测试

> 注意：一些现有的测试使用 RUN：在其他文件中为 true，而不是仅将其他文件放在 Inputs/目录中。不建议使用此模式

### 易碎测试

编写易碎的测试很容易，如果被测试的工具将完整路径输出到输入文件，则该测试会虚假地失败。例如，默认情况下，opt 输出一个 ModuleID
```sh
$ cat example.ll
define i32 @main() nounwind {
    ret i32 0
}

$ opt -S /path/to/example.ll
; ModuleID = '/path/to/example.ll'

define i32 @main() nounwind {
    ret i32 0
}
```
ModuleID可能与CHECK行意外匹配。例如：
```c
; RUN: opt -S %s | FileCheck

define i32 @main() nounwind {
    ; CHECK-NOT: load
    ret i32 0
}
```
如果放置在下载目录中，此测试将失败

为了使测试更可靠，请始终在RUN行中使用 `opt ... < %s`。当输入来自 stdin 时，opt不输出ModuleID。

### 特定平台的测试
每当添加需要特定平台知识（与生成的代码，特定输出或后端功能相关）的测试时，都必须确保隔离这些功能，以使在不同架构上运行的构建机器人（甚至不进行编译）所有后端），请不要失败

第一个问题是检查特定于目标的输出，例如结构的大小，路径和体系结构名称，例如
- 包含 Windows 路径的测试在Linux上将失败，反之亦然。 
- 在文本中某处检查x86_64的测试将在其他任何地方失败。
- 测试调试信息在何处计算类型和结构的大小

另外，如果测试依赖于任何后端编码的任何行为，则它必须位于自己的目录中。因此，例如，ARM的代码生成器测试进入 `test/CodeGen/ARM` 等。这些目录包含一个特殊的 lit 的配置文件，以确保该目录中的所有测试仅在编译了特定的后端且可用后才能运行

例如，在 `test/CodeGen/ARM`上，`lit.local.cfg`为：
```sh
config.suffixes = ['.ll', '.c', '.cpp', '.test']
if not 'ARM' in config.root.targets:
  config.unsupported = True
```
其他特定于平台的测试是那些依赖于特定子架构的特定功能的测试，例如仅依赖于支持AVX2的英特尔芯片

例如，`test/CodeGen/X86/psubus.ll` 测试三个子体系结构变体：
```sh
; RUN: llc -mcpu=core2 < %s | FileCheck %s -check-prefix=SSE2
; RUN: llc -mcpu=corei7-avx < %s | FileCheck %s -check-prefix=AVX1
; RUN: llc -mcpu=core-avx2 < %s | FileCheck %s -check-prefix=AVX2
```
并且检查是不同的：
```c
; SSE2: @test1
; SSE2: psubusw LCPI0_0(%rip), %xmm0
; AVX1: @test1
; AVX1: vpsubusw LCPI0_0(%rip), %xmm0, %xmm0
; AVX2: @test1
; AVX2: vpsubusw LCPI0_0(%rip), %xmm0, %xmm0
```
因此，如果要测试的行为是特定于平台的或依赖于子体系结构的特殊功能，则必须添加特定的三元组，使用特定的FileCheck进行测试，然后将其放入要过滤的特定目录中所有其他架构。

### 限制测试执行
某些测试只能在特定的配置中运行，例如在调试版本中或在特定平台上。使用 `REQUIRES` 和 `UNSUPPORTED` 来控制何时启用测试。

某些测试可能会失败。例如，可能存在测试检测到的已知错误。使用 `XFAIL` 将测试标记为预期的失败。如果执行失败，则 `XFAIL` 测试将成功，如果执行失败，则将失败。
```c
; This test will be only enabled in the build with asserts.
; REQUIRES: asserts
; This test is disabled on Linux.
; UNSUPPORTED: -linux-
; This test is expected to fail on PowerPC.
; XFAIL: powerpc
```
`REQUIRES` 和 `UNSUPPORTED` 和 `XFAIL` 都接受以逗号分隔的布尔表达式列表。每个表达式中的值可以是：
- 通过配置文件（如lit.cfg）添加到 config.available_features 中的功能。 
- 目标三元组的子字符串（仅 UNSUPPORTED 和 XFAIL）

如果所有表达式均为真，则 REQUIRES 启用测试。 <br>
如果任何表达式为真，则 UNSUPPORTED 禁用测试。 <br>
如果任何表达式为真，XFAIL 期望测试失败<br>

作为特殊情况，XFAIL：*预计到处都会失败
```c
; This test is disabled on Windows,
; and is disabled on Linux, except for Android Linux.
; UNSUPPORTED: windows, linux && !android
; This test is expected to fail on both PowerPC and ARM.
; XFAIL: powerpc || arm
```

### 替换

除了替换LLVM工具名称之外，在RUN行中还执行以下替换：
- %%
    - 替换为单个％。这允许转义其他替换
- %s
    - 测试用例来源的文件路径。这适合将命令行作为LLVM工具的输入传递
    - 如： /home/user/llvm/test/MC/ELF/foo_test.s
- %S
    - 测试案例来源的目录路径
    - 如： /home/user/llvm/test/MC/ELF
- %t
    - 可以用于此测试用例的临时文件名的文件路径。文件名不会与其他测试用例冲突。如果您需要多个临时工，则可以附加到该工时。这对于某些重定向输出的目标很有用
    - 如： /home/user/llvm.build/test/MC/ELF/Output/foo_test.s.tmp
- %T
    - %t的目录。不推荐使用。不应该使用它，因为它很容易被滥用，并且会导致两次测试之间出现竞争状况
    - 如果需要临时目录，请改用 rm -rf %t && mkdir %t
    - 如： /home/user/llvm.build/test/MC/ELF/Output
- %{pathsep}
    - 扩展到路径分隔符，即 `:`（或Windows上的`;`）

- %/s, %/S, %/t, %/T:
    - 就像上面的相应替换一样，但是用/替换任何\字符。这对标准化路径分隔符很有用。
    - 如: %s:  C:\Desktop Files/foo_test.s.tmp
    - 如: %/s: C:/Desktop Files/foo_test.s.tmp
- %errc_<ERRCODE>
    - 可以替换某些错误消息，以允许基于主机平台的不同拼写
        - 当前支持以下错误代码：ENOENT，EISDIR，EINVAL，EACCES
            - 如: Linux %errc_ENOENT: No such file or directory
            - 如: Windows %errc_ENOENT: no such file or directory

__clang特定的替换：__

- %clang
    - 调用 Clang 驱动程序
- %clang_cpp
    - 调用C ++的Clang驱动程序
- %clang_cl
    - 调用兼容CL的Clang驱动程序
- %clangxx
    - 调用 G++ 兼容的 Clang 驱动程序
- %clang_cc1
    - 调用Clang前端
- %itanium_abi_triple, %ms_abi_triple
    - 这些替换可用于将当前目标三元组调整为所需的ABI。例如，如果测试套件与i686-pc-win32目标一起运行，则 %itanium_abi_triple 将扩展为 i686-pc-mingw32。这样一来，测试就可以使用特定的ABI进行运行，而不必将其限制为特定的三元组

__FileCheck特定的替代：__
- %ProtectFileCheckOutput
    - 仅当该调用的文本输出影响测试结果时，才应在FileCheck调用之前。这通常很容易分辨：只需查找FileCheck调用的stdout或stderr的重定向或管道

要添加更多替换，请查看 test/lit.cfg 或  lit.local.cfg


### 选项
llvm lit 配置允许使用用户选项自定义某些内容

- llc, opt, …
    - 用自定义命令行替换相应的 llvm 工具名称。这允许为这些工具指定自定义路径和默认参数。例子：
    - % llvm-lit "-Dllc=llc -verify-machineinstrs"
- run_long_tests
    - 启用长时间运行的测试
- llvm_site_config
    - 加载指定的 lit 配置，而不是默认配置

### 其他特性

为了使 RUN 行编写更容易，有几个帮助程序。这些帮助程序在运行测试时位于 PATH 中，因此您可以使用它们的名称来调用它们。例如：

- not
    - 该程序运行其参数，然后从中反转结果代码。零结果代码变为1。非零结果代码变为0

为了使输出更有用，lit 将在测试用例的行中扫描包含与 `PR[0-9]+` 匹配的模式的行。这是用于指定与测试用例相关的 PR（问题报告）编号的语法。 `PR`之后的数字指定 LLVM Bugzilla编号。指定PR号后，它将在通过/失败报告中使用。当测试失败时，这对于快速获取某些上下文很有用

最后，任何包含“ END”的行。将导致对行的特殊解释终止。通常是在最后一个RUN：行之后立即执行此操作。这有两个副作用：
- 它可以防止对测试程序中的行进行特殊解释，而不是对测试用例的指令进行解释，并且 
- 它通过避免解释文件的其余部分来加快大型测试用例的速度。

