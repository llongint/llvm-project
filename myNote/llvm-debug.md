
强烈推荐访问 [原文地址](https://clangbuiltlinux.github.io/llvm-dev-conf-2020/nick/debugging_llvm.html#/) !!!




###  一、重要的编译器通用标志
```{.bash}
-E: 在编译之前停止，在预处理之后产生 .i
-S: 汇编前停止，产生.s
-c: 在链接之前停止，产生.o
-v: 打印要执行的命令并运行
-###: 打印要执行命令，但不运行
-o -: 打印到标准输出，而不是将输出写入文件
```


调试驱动程序搜索路径: [log](search-path.log)

```{.bash}
......
#include "..." search starts here:
#include <...> search starts here:
 /usr/lib/gcc/arm-linux-gnueabihf/8/../../../../include/c++/8
 /usr/lib/gcc/arm-linux-gnueabihf/8/../../../../include/arm-linux-gnueabihf/c++/8
 /usr/lib/gcc/arm-linux-gnueabihf/8/../../../../include/c++/8/backward
 /usr/local/include
 /home/pi/software/clang+llvm-11.0.0-armv7a-linux-gnueabihf/lib/clang/11.0.0/include
 /usr/include/arm-linux-gnueabihf
 /usr/include
 ......
```

我的二进制是用 `clang` 编译的吗？
```{.bash}
pi@raspberrypi:~/llvmTest $ llvm-readelf --string-dump=.comment foo.o
String dump of section '.comment':
[     1] clang version 11.0.0 (http://git.linaro.org/toolchain/jenkins-scripts.git 9d4a8b9be7e552b21f832614d895e31c014486e7)
```

### 二、一些用处很大的编译选项

- -Xclang \<arg\>
    - 传递 <arg> 给 clang 编译器
        - 具体的参数信息参见 `clang -Xclang -help -E foo.cpp`
        - `-dump-tokens` : 运行预处理器，转储tokens的内部rep
            ```{.bash}
            clang -Xclang -dump-tokens -E foo.cpp
            ```
        - `-ast-dump`：生成AST，然后调试转储它们
            ```{.bash}
            clang -Xclang -ast-dump -E foo.cpp
            ```
        - `-emit-html`: 打印所有选项
            ```{.bash}
            clang -Xclang -emit-html -o foo.html foo.cpp
            ```
- 图像画方式查看 AST
    ```{.bash}
    $ sudo apt install xdot
    $ clang++ -Xclang -ast-view -E foo.cpp
    ```
    <img src="./picture/xdot-ast.png" alt="图片替换文本" width="467" height="219" align="bottom" />
    
    - _`注`_：构建时需要使用 debug 版本 `-DCMAKE_BUILD_TYPE=Debug.`

### 三、将LLVM IR转储
- 命令：
    ```{.bash}
    clang++ foo.cpp -emit-llvm -S -o - -fno-discard-value-names -g0
    ```
    - -emit-llvm: 输出 LLVM IR
    - -S: 输出人眼可读的 foo.ll, 而非二进制的 foo.bc
    - -o -: 输出到标准输出, 而不是输出到 .ll or .bc 文件.
    - -fno-discard-value-names: 不要在LLVM IR中丢弃值名称.
        - ![](./picture/no-discard-value-names.png)
    - -g0: IR 中输出更少的调试信息.

- 转储 LLVM IR 
    - 如果下一个调试步骤是 `opt`，则使用以下命令转储:
        ```{.bash}
        -O2 -Xclang -disable-llvm-passes
        ```
    - 如果下一个调试步骤是 `llc`，则使用以下命令转储:
        ```{.bash}
        -O2
        ```

- `-disable-llvm-passes`: 通过根本不运行任何LLVM传递来从前端获取原始LLVM IR
    ```{.bash}
    clang++ foo.cpp -emit-llvm -S -Xclang -disable-llvm-passes
    ```
    - ![](./picture/disable-llvm-passes.png)
- `-S`: 仅运行预处理和编译步骤
    ```{.bash}
    clang++ foo.cpp -emit-llvm -S -O2
    ```
- `-O0`：IR将添加 optnone 函数属性，从而阻止运行任何优化
    ![](./picture/optnone.png)


---
### 四、使用 opt 优化
- `-verify` : 模块验证??
![](./picture/verify.png)
- `-verify-each` : 每次转换后验证??
- `-licm` : 循环不变??
![](./picture/licm.png)
- `-print-after-all`: 如果一个 pass 没有修改就不打印
- `-print-before-all`： ???
    ```{.bash}
    $ opt -verify -S foo.ll
    $ opt -O2 -S foo.ll
    $ opt -O2 -verify-each -S foo.ll
    ```
    ```{.bash}
    $ clang++ -emit-llvm -S -O2 -Xclang -disable-llvm-passes foo.cpp
    $ opt -O2 -S foo.ll
    ```
    - 注：一般应匹配 `clang++ -emit-llvm -S -O2 foo.cpp`, 尽管前端可以选择自己的传输管道
- 优化举例：
    ```{.bash}
    $ clang++ -emit-llvm -S -O2 -Xclang -disable-llvm-passes foo.cpp
    $ opt -licm -S foo.ll
    ```
    ```{.bash}
    #include <stddef.h>
    void foo (int* a, int x, int y) {
    for (size_t i = 0; i < 100; ++i)
        a[i] = x + y;
    }
    ```
    - 优化前：
        ```{.bash}
        for.body:                                         ; preds = %for.cond
        %3 = load i32, i32* %x.addr, align 4, !tbaa !6
        %4 = load i32, i32* %y.addr, align 4, !tbaa !6
        %add = add nsw i32 %3, %4
        %5 = load i32*, i32** %a.addr, align 8, !tbaa !2
        %6 = load i64, i64* %i, align 8, !tbaa !8
        %arrayidx = getelementptr inbounds i32, i32* %5, i64 %6
        store i32 %add, i32* %arrayidx, align 4, !tbaa !6
        br label %for.inc
        ```
    - 优化后：
        ```{.bash}
        for.body:                                         ; preds = %for.cond
        %arrayidx = getelementptr inbounds i32, i32* %3, i64 %inc1
        store i32 %add, i32* %arrayidx, align 4, !tbaa !6
        br label %for.inc
        ```
- 找出opt出现问题的地方(spotting)
    ```{.bash}
    $ opt -O2 -print-after-all -S foo.ll
    # -print-after-all doesn't print a pass if it did no modifications.
    $ opt -O2 -print-before-all -S foo.ll
    ```
    ```{.bash}
    *** IR Dump Before Module Verifier ***
    *** IR Dump Before Instrument function entry/exit with calls to e.g. mcount() (pre inlining) ***
    *** IR Dump Before Simplify the CFG ***
    *** IR Dump Before SROA ***
    *** IR Dump Before Early CSE ***
    *** IR Dump Before Lower 'expect' Intrinsics ***
    *** IR Dump Before Force set function attributes ***
    ```

---
### 五、使用 LLC 进行底层优化
- llvm .bc-> .bc模块化优化器和分析打印机
    ```{.bash}
    $ llc foo.ll # produces foo.s
    $ llc -filetype=obj foo.ll # produces foo.o
    $ llc --print-after-all foo.ll
    ```
    ```{.bash}
    *** IR Dump After Pre-ISel Intrinsic Lowering ***
    *** IR Dump After Expand Atomic instructions ***
    *** IR Dump After Module Verifier ***
    *** IR Dump After Canonicalize natural loops ***
    *** IR Dump After Loop Strength Reduction ***
    ```
- `llc -debug-pass=Structure foo.ll`
    ```{.bash}
    ...
    Target Library Information
    Target Pass Configuration
    Machine Module Information
    Target Transform Information
    Type-Based Alias Analysis
    Scoped NoAlias Alias Analysi
    ```
- 指令选择（ISEL）
    - 前：
        ```{.bash}
        define dso_local void @_Z3fooPiii(i32* nocapture %a, i32 %x, i32 %y) local_unnamed_addr #0 {
        entry:
        %add = add nsw i32 %y, %x
        %broadcast.splatinsert5 = insertelement <4 x i32> undef, i32 %add, i32 0
        ...
        ```
    - 前：
        ```{.bash}
        # *** IR Dump After Finalize ISel and expand pseudo-instructions ***:
        bb.0.entry:
        liveins: $rdi, $esi, $edx
        %2:gr32 = COPY $edx
        %1:gr32 = COPY $esi
        %0:gr64 = COPY $rdi
        %3:gr32 = nsw ADD32rr %2:gr32(tied-def 0), %1:gr32, implicit-def dead $eflags
        %4:vr128 = MOVDI2PDIrr killed %3:gr32
        %5:vr128 = PSHUFDri killed %4:vr128, 0
        ...
        ```
- 寄存器分配
    ```{.bash}
    0B      bb.0.entry:
            liveins: $edx, $esi, $rdi
    80B       renamable $esi = nsw ADD32rr killed renamable $esi(tied-def 0), killed renamable $edx, implicit-def dead $eflags
    96B       renamable $xmm0 = MOVDI2PDIrr killed renamable $esi
    112B      renamable $xmm0 = PSHUFDri killed renamable $xmm0, 0
    ```

### LLVM中的PRINTF调试






