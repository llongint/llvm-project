[TOC]
# [UG3编译技术 2019/2020](https://www.inf.ed.ac.uk/teaching/courses/ct/19-20/)

上年课程内容：[2018-19 网页](https://www.inf.ed.ac.uk/teaching/courses/ct/18-19/)

## 说明

这是一门纯课程，无需考试。该课程将包括两个作业。第一个将针对Java的MIPS指令集体系结构从头开始开发编译器。第二项任务是关于在现有的 C++（LLVM）编译器基础结构中实现编译器 pass。 

您的分数仅仅是您的编译器能够正确编译的测试程序数量的函数。我们每天都会通过自动测试框架对您的课程进行评估，使您能够始终了解自己的进度。

## 课程资料

- 授课笔记
    - [本单元](#教学大纲) [教学大纲](https://www.inf.ed.ac.uk/teaching/courses/ct/19-20/syllabus.html)
    - 1: Introduction
    - 2: The View from 35,000 Feet
    - 3: Lexical Analysis - An Introduction
    - 4: Automatic Lexer Generation
    - 5: Top-down Parsing
    - 6 : Dealing with Ambiguity + Bottom-up Parsing
    - 7 : Abstract Syntax
    - 8 : Semantic Analysis: Name Analysis
    - 9 : Semantic Analysis: Types
    - 10 : Introduction to Assembly
    - 11 : Introduction to Code Generation
    - 12 : Code Shape, Memory management and Function calls
    - 13 : Dataflow Analysis
    - 14 : Liveness, dominators and SSA
    - 15 : Instruction Selection
    - 16 : Instruction Scheduling
    - 17 : Register Allocation
    - [LLVM-1: 简介](###LLVM-1、简介)
    - [LLVM-2: 手写一个 pass](#LLVM-2:手写一个pass)
    - LLVM-3: Iterating and Casting
    - LLVM-4: Dead code elimination
    - LLVM-5: Liveness Analysis
- Course information
    - http://course.inf.ed.ac.uk/ct/
- Informatics policies
    - [Late coursework policy](http://web.inf.ed.ac.uk/infweb/student-services/ito/admin/coursework-projects/late-coursework-extension-requests)
    - [Academic conduct policy](http://web.inf.ed.ac.uk/infweb/admin/policies/academic-misconduct)

---
## CS3编译技术：教学大纲

本课程描述了现代编程语言编译器的各个阶段，并着重介绍了广泛使用的技术。

### 环境

除INF2C之外，没有其他正式的先决条件。

### 教学大纲
- 简介：编译器的结构
- 词法分析：标记，正则表达式，Lex
- 解析：无上下文语法，预测和 LR 解析，Yacc
- 抽象语法：语义动作，抽象解析树
- 语义分析：符号表，绑定，类型检查
- 堆栈框架：表示和抽象
- 中级代码：表示树，翻译
- 基本块和 traces：canonical tree 和 条件分支。 
- 指令选择：选择算法，RISC 和 CISC。 
- 活动性分析：solution of dataflow equations. 
- 寄存器分配：colouring by simplification, coalescing.
- 高级主题
---

### 1、简介
---
### 2: The View from 35,000 Feet
---
### 3: Lexical Analysis - An Introduction
---
### 4: Automatic Lexer Generation
---
### 5: Top-down Parsing
---
### 6 : Dealing with Ambiguity + Bottom-up Parsing
---
### 7 : Abstract Syntax
---
### 8 : Semantic Analysis: Name Analysis
---
### 9 : Semantic Analysis: Types
---
### 10 : Introduction to Assembly
---
### 11 : Introduction to Code Generation
---
### 12 : Code Shape, Memory management and Function calls
---
### 13 : Dataflow Analysis
---
### 14 : Liveness, dominators and SSA
---
### 15 : Instruction Selection
---
### 16 : Instruction Scheduling
---
### 17 : Register Allocation
---
### LLVM-1、简介

#### 项目概况
- 使用 C++ 编写
    - 但是没有模板和艰深的代码
    - 如果您知道 C 或 Java 就 OK 了
- LLVM源代码托管在GitHub上
    - 您需要将最终项目提交给Gitlab
- 项目将在 linux 上评估
    - LLVM可在OS X和Windows上运行，但我们只会在Linux上进行评估
    - 如果您在其他平台上工作，请确保它也可以在Linux上工作！
    - 我们将为您提供要构建的git hash

#### 开始使用
- 阅读原始的LLVM论文（可选）
    -  LLVM: A Compilation Framework for Lifelong Program Analysis & Transformation, Chris Lattner and Vikram Adve, CGO 2004
    - https://dl.acm.org/citation.cfm%3Fid=977673
- 阅读有关LLVM的Dr Dobbs文章（可选）
    - The Design of LLVM, Chris Lattner, 2012
    - https://www.drdobbs.com/architecture-and-design/the-design-of-llvm/240001128
- 查阅 [LLVM.org](https://llvm.org/)

#### 什么是 LLVM
- 一个构建工具的开源框架
    - 通过将LLVM项目和您自己的库提供的各种库链接在一起来创建工具
- 一个可扩展的强类型中间表示，即LLVM IR
    - [https://llvm.org/docs/LangRef.html](https://llvm.org/docs/LangRef.html)
- 具有工业实力的 C/C++ 优化编译器
    - 您可能知道 clang/clang++，但这些实际上只是驱动程序，它们调用LLVM的不同部分（库）
#### LLVM 的历史
- 由UIUC〜2000的Chris Lattner发起
    - 最初的商业用途是在Apple的OS X上作为OpenGL Jitter使用
- 多年发展成为一个完整的C / C ++编译器，直到最近才需要GCC的某些部分
    - llvm-gcc
- 当今世界上LLVM的许多用途
    - OS X和iOS（XCode）平台编译器
    - FreeBSD平台编译器
    - Google Android NDK编译器
    - ARM参考编译器
    - Microsoft DirectX shader 编译器
    - NVIDIA CUDA编译器
#### 典型的优化编译器
<img src=./picture/typical-optimize-compiler.png width=50% />

#### LLVM 优化编译器
<img src=./picture/llvm-optimize-compiler.png width=60% />

#### LLVM 提供了哪些工具
- 太多了!! clang，opt，llc，lld 只是其中的四个
#### LLVM支持哪些优化
- 太多了!! 让我们通过运行 `opt --help` [来查看](./opt.help)
#### 如何获取LLVM源
- LLVM托管在GitHub的llvm-project git 仓库中
    - 对于此类，您将需要使用 `clang` 和 `llvm`
- 选择一个目录以将存储库克隆到
    ```{.bash}
    $ cd directory-to-clone-into
    $ git clone https://github.com/llvm/llvm-project.git
    ```
#### 如何构建 LLVM
- LLVM需要Cmake版本3.4.2+来生成构建文件
    - 最新版本的Cmake已安装在DICE上
- 默认情况下，Cmake会生成构建文件的调试版本，该版本将以最低的优化级别编译LLVM，并启用断言和调试符号
    - 最容易调试，但编译大型程序的速度较慢，并占用最多的磁盘空间
- Cmake支持多种构建系统
    - make, XCode, Visual Studio, Ninja 等
- 在LLVM源目录之外为您的构建创建一个新目录
    ```{.bash}
    $ cd llvm-project ; mkdir build ; cd build
    $ cmake ../llvm
    $ cmake --build .
    ```
#### 如何更快地构建 LLVM
- 默认情况下，会构建 llvm-project git 仓库中的每个工具
- 我们仅通过构建X86目标就可以加快速度
    -  -DLLVM_TARGETS_TO_BUILD=X86
- 并且仅构建LLVM（始终构建）和clang
    - -DLLVM_ENABLE_PROJECTS=clang
    ```{.bash}
    $ cd llvm-project ; mkdir build ; cd build
    $ cmake ../llvm -D LLVM_TARGETS_TO_BUILD=X86 -DLLVM_ENABLE_PROJECTS=clang
    $ cmake --build .
    ```
#### 如何从源代码生成LLVM IR
- 要生成LLVM IR，请使用带有 `-emit-llvm` 选项的 clang
    - `–S` 生成一个文本文件，`–c` 生成一个二进制文件
        - `clang foo.cpp –emit-llvm -S`
        - `clang foo.cpp –emit-llvm -c`
- 要将二进制文件（.bc）转换为文本文件（.ll），请使用llvm反汇编程序
    - `llvm-dis foo.bc`
- 要将文本文件（.ll）转换为二进制文件（.bc），请使用llvm汇编程序
    - `llvm-as foo.ll`
#### 让我们仔细看看 `LLVM IR`
- LLVM IR 的一些特征
    - 类似于RISC的指令集
    - 强类型
    - 明确的控制流程
    - 使用具有无限临时数（％）的虚拟寄存器集
    - 以静态单一分配形式
    - 机器详细信息摘要(Abstracts machine details)，例如调用约定和堆栈引用
- LLVM IR参考在线
    - https://llvm.org/docs/LangRef.html

- 一个简单示例：

    <img src=./picture/llvm-IR-test.png width=60% />

#### 优化 LLVM-IR
- 上一节引用的 LLVM IR 不是最佳的
- 我们知道程序通过查看返回1
- 让我们用 `opt` 优化位码
    - 默认情况下，`opt` 不起任何作用，必须指定一个优化，例如 `-O2`
        <img src=./picture/opt-test.png width=60% />
#### 从 LLVM IR生成机器代码
- 使用 `llc`


---
### LLVM-2:手写一个pass

#### Passes
- LLVM在目标程序上应用分析和转换的中间表示(LLVM applies a chain of analyses and transformations on the target program.)
- 这些分析或变换中的每一个都被称为一个 Pass
- 我们已经见过了一些 pass，比如 mem2reg，early-cse, constprop
- 一些 pass 是与机器无关的，invoked by opt
- 有些 pass 是机器相关的，由 llc 援引(invoked)
- 一些 pass 需要其他 pass 提供信息，这种依赖关系必须明确说明
    - 比如：一个常见的模式是 一个转换(transformation) pass 需要一个 分析(analysis) Pass
#### 不同类型的 pass
- pass 可以是 llvm class pass 的一个实例
- 下面例举了很多种类的 pass
    ```{.hzq}
    function pass --------.
    module pass ----------|
    region pass ----------|
    call graph SCC pass --+--> pass
    Loop pass ------------|
    basic block pass -----|
    ...... ---------------.
    ```
#### 程序中操作码的个数
```
让我们写一个 pass，计算每个操作码出现在给定函数中的次数。 此pass必须为每个函数打印一个列表，其中包含所有在其代码中显示的指令，其次是使用这些操作码的次数。
```
```{.cpp}
int foo(int n, int m) {
    int sum = 0;
    int c0;
    for (c0 = n; c0 > 0; c0--) {
        int c1 = m;
        for (; c1 > 0; c1--) {
            sum += c0 > c1 ? 1 : 0;
        }
    }
    return sum;
}
```
- 我们的 pass 为程序中的每个函数运行一次，因此，它是一个函数 pass。如果我们想看到整个程序，那么我们必须实现一个 ModulePass


## 没法编译，计划夭折


---
### LLVM - 3: Iterating and Casting
---
### LLVM - 4: Dead code elimination
---
### LLVM - 5: Liveness Analysis
---

























